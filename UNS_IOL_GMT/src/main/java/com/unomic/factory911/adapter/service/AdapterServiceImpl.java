package com.unomic.factory911.adapter.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.factory911.adapter.domain.AdapterVo;
import com.unomic.factory911.device.domain.DeviceStatusVo;
import com.unomic.factory911.device.domain.DeviceVo;

@Service
@Repository
public class AdapterServiceImpl implements AdapterService{

	private final static String ADAPTER_SPACE= "com.factory911.adapter.";
	private final static String DEVICE_SPACE= "com.factory911.device.";
	private static final Logger logger = LoggerFactory.getLogger(AdapterServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	

	@Override
	public AdapterVo getLastInputData(AdapterVo inputVo){

		AdapterVo newVo = new AdapterVo();
		newVo.setDvcId(inputVo.getDvcId());

		Integer cnt =  (Integer) sql_ma.selectOne(ADAPTER_SPACE + "cntAdapterStatus",inputVo);

		if(1 < cnt){
			newVo  = (AdapterVo) sql_ma.selectOne(ADAPTER_SPACE + "getLastAdapterStatus",inputVo);
		}
    	return newVo;
	}
	
	@Override
	public AdapterVo chkDateStarterAGT(AdapterVo preVo, AdapterVo crtVo){
		if(CommonFunction.isDateStart(preVo.getStartDateTime(), crtVo.getStartDateTime())){
			logger.error("@@@@@start Date Time AGT@@@@@");
			logger.error("startDateTime:"+preVo.getStartDateTime());
			logger.error("endDateTime:"+crtVo.getStartDateTime());
			AdapterVo starterVo = new AdapterVo();
			starterVo.setDvcId(crtVo.getDvcId());
			starterVo.setStartDateTime(CommonFunction.getStandardHourToday());
			starterVo.setChartStatus(CommonCode.MSG_DATE_START);
			starterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));

			editLastEndTime(starterVo);
			addPureStatus(starterVo);
			
			return starterVo;
		}
		return null;
		
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String editLastEndTime(AdapterVo firstOfListVo)
	{
		int cnt = (Integer) sql_ma.selectOne(ADAPTER_SPACE + "cntLastAdapterStatus", firstOfListVo);
		if(cnt==0){
		}else{
			AdapterVo tmpAdapterVo = (AdapterVo) sql_ma.selectOne(ADAPTER_SPACE + "getLastStartTime", firstOfListVo);
			tmpAdapterVo.setSender(firstOfListVo.getSender());
			if(cnt>1){
				sql_ma.delete(ADAPTER_SPACE + "removeExceptionEndTime",tmpAdapterVo);
			}
			
			tmpAdapterVo.setEndDateTime(firstOfListVo.getStartDateTime());

			sql_ma.update(ADAPTER_SPACE + "editLastEndTime", tmpAdapterVo);
			
		}

		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addPureStatus(AdapterVo pureStatusVo)
	{
		
		logger.info("addPureStatus:"+pureStatusVo.getDvcId());
		sql_ma.insert(ADAPTER_SPACE+"addPureData", pureStatusVo);
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addListPureStatus(List<AdapterVo> listPureStatus)
	{	
		HashMap <String, Object> dataMap = new HashMap<String, Object>();
		
		dataMap.put("listPureStatus", listPureStatus);
		sql_ma.insert(ADAPTER_SPACE + "addListPureData", dataMap);

		return "OK";
	}
	
	//두개의 다른 기능이지만 트랜잭션 때문에 하나로 묶어둠.
	@Override
	@Transactional(value="txManager_ma")
	public String editLastNAddList(List<AdapterVo> listPureStatus, List<DeviceStatusVo> dvcList)
	{
		if(listPureStatus.size()<1){
			return "ZeroSize";
		}
		AdapterVo firstOfListVo = listPureStatus.get(0);
		
		//기존 데이터 수량 체크해서 0일 경우에 는 아무것도 안함. 0 이 아니면 udpate.
		int cnt = (Integer) sql_ma.selectOne(ADAPTER_SPACE + "cntLastAdapterStatus", firstOfListVo);
		if(cnt==0){
		}else{
			//remove Time Exception Data
			sql_ma.delete(ADAPTER_SPACE+"removeExceptionData", firstOfListVo);
			
			if(cnt>1){
				//sql_ma.update(ADAPTER_SPACE + "editRecoverDataEndTime",tmpAdapterVo);
				//change to update
				//sql_ma.delete(ADAPTER_SPACE + "removeExceptionEndTime",tmpAdapterVo);
			}
			firstOfListVo.setDvcId(firstOfListVo.getSender());
			AdapterVo tmpAdapterVo = (AdapterVo) sql_ma.selectOne(ADAPTER_SPACE + "getLastStartTime", firstOfListVo);
			if(tmpAdapterVo!=null){
				logger.info("tmpAdapterVo:"+tmpAdapterVo);
				logger.info("firstOfListVo:"+firstOfListVo);
				tmpAdapterVo.setSender(firstOfListVo.getSender());
				tmpAdapterVo.setEndDateTime(firstOfListVo.getStartDateTime());
				sql_ma.update(ADAPTER_SPACE + "editLastEndTime", tmpAdapterVo);
			}
		}
		
		HashMap <String, List<AdapterVo>> dataMap = new HashMap<String, List<AdapterVo>>();
		dataMap.put("listPureStatus", listPureStatus);
		sql_ma.insert(ADAPTER_SPACE + "addListPureData", dataMap);
		
		
		
		//일일이 넣지 말고 한번에 넣기.
		HashMap <String, List<DeviceStatusVo>> dataMapDvc = new HashMap<String, List<DeviceStatusVo>>();
		
		//sql_ma.insert(DEVICE_SPACE + "addListDvcStatus", dataMapDvc);
		
		DeviceStatusVo inputVo = dvcList.get(0);
		
		int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE +"cntLastDvcStatus", inputVo);
		Iterator <DeviceStatusVo> it =  dvcList.iterator();
		
		if(cntDvc < 1){//대상 장비 누적값 없을경우 바로 추가.
			dataMapDvc.put("listDvcStatus", dvcList);
			sql_ma.insert(DEVICE_SPACE + "addListDvcStatus", dataMapDvc);
		}else{// 기존값 있을경우 중복 체크해서 결정.
			DeviceStatusVo lastVo = (DeviceStatusVo) sql_ma.selectOne(DEVICE_SPACE +"getLastDvcStatus", inputVo);
			logger.info("lastVo:"+lastVo);
			
			// Compare with finalList and dvcList.
			List<DeviceStatusVo> finalList = new ArrayList<DeviceStatusVo>();
			finalList.add(lastVo);
			while(it.hasNext()){
				DeviceStatusVo crtDvcVo = it.next();
				if(crtDvcVo.getChartStatus().equals(finalList.get(finalList.size()-1).getChartStatus())	){
					continue;
				}else{
					logger.info("crtDvcVo:"+crtDvcVo);
					finalList.add(crtDvcVo);
				}
			}
			
			finalList.remove(0);
			if(finalList.size()>0){
				
				dataMapDvc.put("listDvcStatus", finalList);
				sql_ma.update(DEVICE_SPACE + "setEndTmDvcStatus", finalList.get(0) );
				sql_ma.insert(DEVICE_SPACE + "addListDvcStatus", dataMapDvc);
			}
			
		}
		
		// 최적화 필요. 리스트하나당 쿼리가 너무 많음.?
//		while(it.hasNext()){
//			//sql_ma.update(DEVICE_SPACE +"adjustDeviceChartStatus_SP", it.next());
//			DeviceStatusVo inputVo = it.next();
//			//logger.error("[cannon]inputVo : " + inputVo);
//			
//			int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE +"cntLastDvcStatus", inputVo);
//			
//			if(cntDvc < 1){//대상 장비 누적값 없을경우 바로 추가.
//				sql_ma.insert(DEVICE_SPACE +"addDvcStatus", inputVo );
//			}else{// 기존값 있을경우 중복 체크해서 결정.
//				//case1 상태 다를경우 추가.
//				DeviceStatusVo rtnVo = (DeviceStatusVo) sql_ma.selectOne(DEVICE_SPACE +"getLastDvcStatus", inputVo);
//				//logger.error("[cannon]rtnVo : " + rtnVo);
//				if(! rtnVo.getLastChartStatus().equals(inputVo.getChartStatus())){
//					sql_ma.update(DEVICE_SPACE +"editRecoverDvcStatus", rtnVo );
//					sql_ma.update(DEVICE_SPACE +"editLastDvcStatus", rtnVo );
//					sql_ma.insert(DEVICE_SPACE +"addDvcStatus", inputVo );
//				}
//			}
//		}
		
		return "OK";
	}
	
	
	@Override
	public List<AdapterVo> getListWinAgent()
	{
		List<AdapterVo> rtnList  = sql_ma.selectList(ADAPTER_SPACE + "getListWinAgent");
		return rtnList;
	}
	
	private AdapterVo isDuple(AdapterVo preVo, AdapterVo inputVo){
		Long preStartTime = 0L;
		if(null == preVo.getStartDateTime()){
			preStartTime = CommonFunction.dateTime2Mil(preVo.getStartDateTime());
		}else{
			preStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		}
		Long crtStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		
		if(crtStartTime<preStartTime){
			logger.error("timeTrash data");
			return null;
		}

		if(preVo.getAlarmNum1().equals(inputVo.getAlarmNum1())
			&&preVo.getAlarmNum2().equals(inputVo.getAlarmNum2())
			&&preVo.getAlarmNum3().equals(inputVo.getAlarmNum3())
			&&preVo.getFdOvrd().equals(inputVo.getFdOvrd())
			&&preVo.getMode().equals(inputVo.getMode())
			&&preVo.getIsZeroSpdLoad().equals(inputVo.getIsZeroSpdLoad())
			&&preVo.getIsZeroFdOvrd().equals(inputVo.getIsZeroFdOvrd())
			&&preVo.getIsZeroActFd().equals(inputVo.getIsZeroActFd())
			&&preVo.getPrgmHead().equals(inputVo.getPrgmHead())
			&&preVo.getMainPrgmName().equals(inputVo.getMainPrgmName())
			&&preVo.getMdlM1().equals(inputVo.getMdlM1())
			&&preVo.getMdlM2().equals(inputVo.getMdlM2())
			&&preVo.getMdlM2().equals(inputVo.getMdlM3())
		)
		{
			logger.info("Duple");
	    	return null;
		}else{
    		return inputVo;
		}
	}

	public String editLastDvcStatus(DeviceVo inputVo)
	{
		logger.error("RUN editLastDvcStatus:"+inputVo);
		sql_ma.update(DEVICE_SPACE + "editDvcLastStatus", inputVo);
	
		return "OK";
	}
	
	
	
	private String genJsonAlarm(String strAlarm1, String strAlarm2){
		//private String genJsonAlarm(String alarmCode1, String alarmMsg1, String alarmCode2,String alarmMsg2){
			//[{"alarmMsg":"FEED SWITCH 0%","alarmCode":"2204"},{"alarmMsg":"FEED SWITCH 0%","alarmCode":"2204"}]
			logger.info("strAlarm1:"+strAlarm1);
			logger.info("strAlarm2:"+strAlarm2);
			
			JSONArray innerArray = new JSONArray();
			JSONObject innerObject1 = new JSONObject();
			JSONObject innerObject2 = new JSONObject();
			
			String[] arrAlarm1;
			String[] arrAlarm2;
			String alarmMsg1="";
			String alarmCode1="";
			String alarmMsg2="";
			String alarmCode2="";
			if(strAlarm1==null ){

			}else if(!strAlarm1.isEmpty()){
				arrAlarm1 = strAlarm1.split(" ");
				alarmCode1 = arrAlarm1[0];
				alarmMsg1 = arrAlarm1[1];
			}
			if(strAlarm2==null ){
				
			}else if(strAlarm2==null || !strAlarm2.isEmpty()){
				arrAlarm2 = strAlarm2.split(" ");
				alarmCode2 = arrAlarm2[0];
				alarmMsg2 = arrAlarm2[1];
			}

			
			innerObject1.put("ALARM_MSG", alarmMsg1);
			innerObject2.put("ALARM_MSG", alarmMsg2);
			
			innerObject1.put("ALARM_CODE", alarmCode1);
			innerObject2.put("ALARM_CODE", alarmCode2);
			
			innerArray.add(innerObject1);
			innerArray.add(innerObject2);
			
			return innerArray.toString();
		}
		
		private String genJsonModal(String modal){
			String[] arrModal = modal.split(" ");
			List<String> arrM = new ArrayList<String>();
			int size = arrModal.length;
			
			String gCode = "";
			String tCode = "";
			for(int i = 0 ; i<size ; i++){
				
				if(arrModal[i].charAt(0)=='G'){
					gCode = arrModal[i].substring(1);
				}else if(arrModal[i].charAt(0)=='T'){
					tCode = arrModal[i].substring(1);
				}else if(arrModal[i].charAt(0)=='M'){
					arrM.add(arrModal[i].substring(1));
				}
			}

			JSONObject rootObj = new JSONObject();
			JSONArray arrGmodal = new JSONArray();
			JSONObject objG0 = new JSONObject();
			JSONArray arrAuxCode= new JSONArray();
			JSONObject objM1 = new JSONObject();
			JSONObject objM2 = new JSONObject();
			JSONObject objM3 = new JSONObject();
			JSONObject objT = new JSONObject();
			
			objG0.put("G0", gCode); arrGmodal.add(objG0);
			objT.put("T", tCode); arrAuxCode.add(objT);
			
			for(int i = 0 ; i < arrM.size() ; i++){
				if(i==0){
					objM1.put("M1", arrM.get(i)); arrAuxCode.add(objM1);
				}else if(i==1){
					objM2.put("M2", arrM.get(i)); arrAuxCode.add(objM2);
				}else if(i==2){
					objM3.put("M2", arrM.get(i)); arrAuxCode.add(objM3);
				}
			}

			rootObj.put("G_MODAL", arrGmodal);
			rootObj.put("AUX_CODE", arrAuxCode);
			
			return rootObj.toString();
		}
		
		

}
