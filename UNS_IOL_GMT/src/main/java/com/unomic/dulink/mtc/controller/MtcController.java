package com.unomic.dulink.mtc.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.mtc.domain.MTConnect;
import com.unomic.dulink.mtc.domain.RogerParser;
import com.unomic.factory911.adapter.domain.AdapterVo;
import com.unomic.factory911.adapter.service.AdapterService;
import com.unomic.factory911.device.domain.DeviceStatusVo;
import com.unomic.factory911.device.domain.DeviceVo;
import com.unomic.factory911.device.service.DeviceService;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/mtc")
@Controller
public class MtcController {
	
	private static final Logger logger = LoggerFactory.getLogger(MtcController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@Autowired
	private DeviceService deviceService;

	@Autowired
	private AdapterService adapterService;

	
	@RequestMapping(value = "getInitTime")
	@ResponseBody
    public String setInitTime(){
		return CommonFunction.getTodayDateTime();
	}
	
	@RequestMapping(value = "timeTest")
	@ResponseBody
    public String timeTest(){
		return CommonFunction.getTodayDateTime();
	}
	
	
	@RequestMapping(value = "dbTest")
	@ResponseBody
	public String dbTest(HttpServletRequest request){
		AdapterVo inputVo = new AdapterVo();
		inputVo.setDvcId("7");
		AdapterVo preVo = adapterService.getLastInputData(inputVo);
		logger.info("preVo:"+preVo);
		return "OK";
	}

	private AdapterVo getDateStarter(AdapterVo preVo, AdapterVo crtVo){
		if(CommonFunction.isDateStart(preVo.getStartDateTime(), crtVo.getStartDateTime())){
			AdapterVo starterVo = new AdapterVo();
			starterVo.setDvcId(crtVo.getDvcId());
			starterVo.setStartDateTime(CommonFunction.getStandardHourToday());
			starterVo.setChartStatus(CommonCode.MSG_DATE_START);
			starterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));
			starterVo.setSender(crtVo.getSender());
			//최근 데이터 endTime edit.
			//editLastEndTime(starterVo);
			//DateStarter input.
			//addPureStatus(starterVo);
			return starterVo;
		}else{
			return null;
		}
	}
	
	// 
	//private List<String> getListInputList(String inputXml){
	private List<String> getListFromInputXML(String inputXml){
		
		Scanner sc = new Scanner(inputXml).useDelimiter(CommonCode.MSG_XML_DELIMITER);
		
		ArrayList<String> listXml = new ArrayList<String>();
		
		while(sc.hasNext()){
			String tmp = sc.next()+CommonCode.MSG_XML_DELIMITER;
			logger.info("tmp:"+ tmp);
			listXml.add(tmp.trim());
		}
		
		//Iterator<String> it = listXml.iterator();
		//while(it.hasNext()){logger.info("xml:[" + it.next() + "]");}
//		logger.info("listXml.size():"+listXml.size());
//		logger.info("lastListXml.:"+listXml.get(listXml.size()-1));	
//		listXml.remove(listXml.size()-1);
		
		return listXml;
	}
	
	private AdapterVo getAGTStatus(MTConnect mtc){

		AdapterVo pureStatusVo = new AdapterVo();
		
		Long statusUnixSec = CommonFunction.mtcDateTime2Mil(mtc.getCreationTime());
		
		
		pureStatusVo.setStartDateTime(CommonFunction.unixTime2Datetime(statusUnixSec));
		pureStatusVo.setDvcId(mtc.getSender());
		pureStatusVo.setSender(mtc.getSender());
		
		pureStatusVo.setStatus(mtc.getStatus());
		if("IOLOGIK".equals(mtc.getStatus())){
			pureStatusVo.setIoLogik(mtc.getIoLogik());
		}else{
			pureStatusVo.setIoLogik("000000");
		}
				
		pureStatusVo.setMainPrgmName(setLimitStr(mtc.getMainPrgmName(),20));
		pureStatusVo.setCrntPrgmName(setLimitStr(mtc.getCrntPrgmName(),20));
		pureStatusVo.setPrgmHead(setLimitStr(mtc.getPrgmHead(),50));
				
		//logger.info("mtc.getAlarmMsg1():"+mtc.getAlarmMsg1());
		
		pureStatusVo.setAlarmMsg1(setLimitStr(mtc.getAlarmMsg1(),100));
		pureStatusVo.setAlarmMsg2(setLimitStr(mtc.getAlarmMsg2(),100));
		pureStatusVo.setAlarmMsg3(setLimitStr(mtc.getAlarmMsg3(),100));

		pureStatusVo.setAlarmNum1(setLimitStr(mtc.getAlarmNum1(),20));
		pureStatusVo.setAlarmNum3(setLimitStr(mtc.getAlarmNum2(),20));
		pureStatusVo.setAlarmNum2(setLimitStr(mtc.getAlarmNum3(),20));
		
		pureStatusVo.setMdlM1(setLimitStr(mtc.getMdlM1(),11));
		pureStatusVo.setMdlM2(setLimitStr(mtc.getMdlM2(),11));
		pureStatusVo.setMdlM3(setLimitStr(mtc.getMdlM3(),11));
		
		pureStatusVo.setMdlT(setLimitStr(mtc.getMdlT(),11));
		pureStatusVo.setMdlD(setLimitStr(mtc.getMdlD(),11));
		pureStatusVo.setMdlH(setLimitStr(mtc.getMdlH(),11));
		
		if(mtc.getSpdLd() == null
			|| mtc.getSpdLd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdLd().equals("NaN")
				) {
			pureStatusVo.setSpdLd(null);
		}else{
			pureStatusVo.setSpdLd(Float.parseFloat(mtc.getSpdLd()));
			if (Float.valueOf(mtc.getSpdLd()) == 0 ){
				pureStatusVo.setIsZeroSpdLoad(true);
			}else{ //( Float.valueOf(mtc.getSpindle_load()) > 0 ){
				pureStatusVo.setIsZeroSpdLoad(false);
			}
		}
		
		if(mtc.getFdOvrd() == null
			|| mtc.getFdOvrd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdLd().equals("NaN")
			) {
			pureStatusVo.setFdOvrd(null);
		}else{
			pureStatusVo.setFdOvrd(Integer.valueOf(mtc.getFdOvrd()).intValue());
			if (Float.valueOf(mtc.getFdOvrd()) == 0 ){
				pureStatusVo.setIsZeroFdOvrd(true);
			}else{// if(Integer.valueOf(mtc.getFdOvrd()) > 0){
				pureStatusVo.setIsZeroFdOvrd(false);
			}
		}
		
		if(mtc.getRpdOvrd() == null
			|| mtc.getRpdOvrd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdLd().equals("NaN")
				) {
			pureStatusVo.setRpdFdOvrd(null);
		}else{
			pureStatusVo.setRpdFdOvrd(Integer.parseInt(mtc.getRpdOvrd()));
		}
		
		if(mtc.getSpdOvrd() == null
			|| mtc.getSpdOvrd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdLd().equals("NaN")
			) {
			pureStatusVo.setSpdOvrd(null);
		}else{
			pureStatusVo.setSpdOvrd(Integer.parseInt(mtc.getSpdOvrd()));
		}
		
		if(mtc.getActFd() == null
			|| mtc.getActFd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdLd().equals("NaN")
			) {
			pureStatusVo.setActFd(null);
		}else{
			pureStatusVo.setActFd((int)Float.parseFloat(mtc.getActFd()));
			if (Float.valueOf(mtc.getActFd()) == 0 ){
				pureStatusVo.setIsZeroActFd(true);
			}else{// if(Integer.valueOf(mtc.getActual_feed()) > 0){
				pureStatusVo.setIsZeroActFd(false);
			}
		}
		
		if(mtc.getSpdActSpeed() == null
			|| mtc.getSpdActSpeed().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdLd().equals("NaN")
			) {
			pureStatusVo.setSpdActSpeed(null);
		}else{
			pureStatusVo.setSpdActSpeed((int)(Float.parseFloat(mtc.getSpdActSpeed()) ));
		}

		pureStatusVo.setMode(setLimitStr(mtc.getMode(),11));
		
		
		pureStatusVo.setWorkDate(CommonFunction.mil2WorkDate(statusUnixSec));
		if(mtc.getMainPrgmStartTime() == null
				|| mtc.getMainPrgmStartTime().equals(CommonCode.MSG_UNAVAIL)
				|| mtc.getSpdLd().equals("NaN")
		) {
			pureStatusVo.setMainPrgmStartTime(null);
		}else{
			if(mtc.getMainPrgmStartTime().equals("0")){
				pureStatusVo.setMainPrgmStartTime(null);
			}else{
				pureStatusVo.setMainPrgmStartTime(CommonFunction.unixTime2Datetime(Long.parseLong(mtc.getMainPrgmStartTime())));
				logger.info("MTC_MainProgramStartTime:"+mtc.getMainPrgmStartTime());
				logger.info("PURE_MainProgramStartTime:"+pureStatusVo.getMainPrgmStartTime());
			}
		}
		if(mtc.getMainPrgmStartTime2() == null
				|| mtc.getMainPrgmStartTime2().equals(CommonCode.MSG_UNAVAIL)
				|| mtc.getSpdLd().equals("NaN")
		) {
			pureStatusVo.setMainPrgmStartTime2(null);
		}else{
			if(mtc.getMainPrgmStartTime2().equals("0")){
				pureStatusVo.setMainPrgmStartTime2(null);
			}else{
				pureStatusVo.setMainPrgmStartTime2(CommonFunction.unixTime2Datetime(Long.parseLong(mtc.getMainPrgmStartTime2())));
			}
		}
		
		if(mtc.getPartCount() == null
			|| mtc.getPartCount().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getPartCount().equals("NaN")
			) {
			pureStatusVo.setPartCount(null);
		}else{
			pureStatusVo.setPartCount((Integer.parseInt(mtc.getPartCount()) ));
		}
		
		if(mtc.getTotalLineNumber() == null
				|| mtc.getTotalLineNumber().equals(CommonCode.MSG_UNAVAIL)
				|| mtc.getTotalLineNumber().equals("NaN")
			) {
			pureStatusVo.setTotalLineNumber(null);
		}else{
			pureStatusVo.setTotalLineNumber((Integer.parseInt(mtc.getTotalLineNumber()) ));
		}
		
		if(mtc.getLine() == null
				|| mtc.getLine().equals(CommonCode.MSG_UNAVAIL)
				|| mtc.getLine().equals("NaN")
				) {
				pureStatusVo.setLine(null);
		}else{
			pureStatusVo.setLine((Integer.parseInt(mtc.getLine()) ));
		}
		
		pureStatusVo.setBlock(setLimitStr(mtc.getBlock(),50));
		
		
		return pureStatusVo;
	}
	

	
	private AdapterVo isDuple(AdapterVo preVo, AdapterVo inputVo){
		Long preStartTime = 0L;
		if(null == preVo.getStartDateTime()){
			preStartTime = CommonFunction.dateTime2Mil(preVo.getStartDateTime());
		}else{
			preStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		}
		Long crtStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		
		if(crtStartTime<preStartTime){
			logger.error("timeTrash data");
			return null;
		}
		//logger.info("before duple check");
		//logger.info("preVo:"+preVo);
		//logger.info("inputVo:"+inputVo);

		if(preVo.getAlarmNum1().equals(inputVo.getAlarmNum1())
			&&preVo.getAlarmNum2().equals(inputVo.getAlarmNum2())
			&&preVo.getAlarmNum3().equals(inputVo.getAlarmNum3())
			&&preVo.getFdOvrd().equals(inputVo.getFdOvrd())
			&&preVo.getMode().equals(inputVo.getMode())
			&&preVo.getIsZeroSpdLoad().equals(inputVo.getIsZeroSpdLoad())
			&&preVo.getIsZeroFdOvrd().equals(inputVo.getIsZeroFdOvrd())
			&&preVo.getIsZeroActFd().equals(inputVo.getIsZeroActFd())
			&&preVo.getPrgmHead().equals(inputVo.getPrgmHead())
			&&preVo.getMainPrgmName().equals(inputVo.getMainPrgmName())			
			&&preVo.getMdlM1().equals(inputVo.getMdlM1())
			&&preVo.getMdlM2().equals(inputVo.getMdlM2())
			&&preVo.getMdlM3().equals(inputVo.getMdlM3())
		)
		{
			logger.info("Duple");
	    	return null;
		}else{
    		return inputVo;
		}
	}
	
	private String addDeviceChartStatusData(DeviceStatusVo inputVo){

		deviceService.adjustDeviceChartStatus_SP(inputVo);
	
		return "OK";
	}
	
	private AdapterVo setIsZeroStatus(AdapterVo inputVo){
		
		if (Float.valueOf(inputVo.getSpdLd()) == 0 ){
			inputVo.setIsZeroSpdLoad(true);
		}else{
			inputVo.setIsZeroSpdLoad(false);
		}
		
		if (Integer.valueOf(inputVo.getFdOvrd()) == 0 ){
			inputVo.setIsZeroFdOvrd(true);
		}else{
			inputVo.setIsZeroFdOvrd(false);
		}
		
		if (Float.valueOf(inputVo.getActFd()) == 0 ){
			inputVo.setIsZeroActFd(true);
		}else{
			inputVo.setIsZeroActFd(false);
		}
		
		return inputVo;
	}
	
	private int getAlarmCnt(AdapterVo inputVo){
		DeviceVo rtnVo = new DeviceVo();
		
		List<String> listAlarmCode = new ArrayList<String>();
		List<String> listAlarmMsg = new ArrayList<String>();
		
		int rtnCnt=0;
		
		if(! (inputVo.getAlarmNum1() == null || inputVo.getAlarmNum1().equals(CommonCode.MSG_UNAVAIL))){
			listAlarmCode.add(inputVo.getAlarmNum1());
			listAlarmMsg.add(inputVo.getAlarmMsg1());
		}
		
		if(! (inputVo.getAlarmNum2() == null || inputVo.getAlarmNum2().equals(CommonCode.MSG_UNAVAIL))){
			listAlarmCode.add(inputVo.getAlarmNum2());
			listAlarmMsg.add(inputVo.getAlarmMsg2());
		}
		if(! (inputVo.getAlarmNum3() == null || inputVo.getAlarmNum3().equals(CommonCode.MSG_UNAVAIL))){
			listAlarmCode.add(inputVo.getAlarmNum3());
			listAlarmMsg.add(inputVo.getAlarmMsg3());
		}
		
		//fix 160307
		if(listAlarmCode.size() > 0){
			rtnVo.setLastAlarmCode(listAlarmCode.get(listAlarmCode.size()-1));
			rtnVo.setLastAlarmMsg(listAlarmMsg.get(listAlarmMsg.size()-1));
			rtnCnt = listAlarmCode.size();
		}else{
			rtnVo.setLastAlarmCode(CommonCode.MSG_UNAVAIL);
			rtnVo.setLastAlarmMsg(CommonCode.MSG_UNAVAIL);
		}
		
		return rtnCnt;
	}
	
	private MTConnect getMTCfromXML(String inputXML){
		MTConnect mtConnect = new MTConnect();
		//Only ASCII
		//inputXML = inputXML.replaceAll("[^\\x20-\\x7e]", "");
//		logger.info("inputXML:" + inputXML);
		DocumentBuilder db=null;
		try {

			try {
				db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Document doc = db.parse(new InputSource(new ByteArrayInputStream(inputXML.getBytes("utf-8"))));
			RogerParser parser = new RogerParser(doc);
			
			parser.Init();
			parser.execute(mtConnect);
			
			logger.info("CRTime:"+mtConnect.getCreationTime());
			logger.info("Sender:"+mtConnect.getSender());
			logger.info("Status:"+mtConnect.getStatus());
			logger.info("FdOvrd:"+mtConnect.getFdOvrd());
			logger.info("RpdFdOvrd:"+mtConnect.getRpdOvrd());
			logger.info("SpdOvrd:"+mtConnect.getSpdOvrd());

			logger.info("MainPrgmName:"+mtConnect.getMainPrgmName());
			logger.info("MainPrgmStrtTime:"+mtConnect.getMainPrgmStartTime());
			logger.info("MainPrgmStrtTime2:"+mtConnect.getMainPrgmStartTime2());
			logger.info("Mode:"+mtConnect.getMode());
			
			logger.info("Mode:"+mtConnect.getMode());
			logger.info("MdlM1:"+mtConnect.getMdlM1());
			logger.info("MdlM2:"+mtConnect.getMdlM2());
			logger.info("MdlM3:"+mtConnect.getMdlM3());
			logger.info("MdlT:"+mtConnect.getMdlT());
			logger.info("MdlD:"+mtConnect.getMdlD());
			logger.info("MdlH:"+mtConnect.getMdlH());
			
			logger.info("AlarmMsg1"+mtConnect.getAlarmMsg1());
			logger.info("AlarmMsg2"+mtConnect.getAlarmMsg2());
			logger.info("AlarmMsg3"+mtConnect.getAlarmMsg3());
			
			logger.info("AlarmNum1"+mtConnect.getAlarmNum1());
			logger.info("AlarmNum2"+mtConnect.getAlarmNum2());
			logger.info("AlarmNum3"+mtConnect.getAlarmNum3());
			
			logger.info("partCount"+mtConnect.getPartCount());
			logger.info("totalLineNumber"+mtConnect.getTotalLineNumber());
			logger.info("Line"+mtConnect.getLine());
			logger.info("Block"+mtConnect.getBlock());
			
			
			//logger.info("AlarmNum3"+mtConnect.getAlarmNum3());
			
			logger.info("PrgmHead:"+mtConnect.getPrgmHead());
			
			
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error(e.toString());
		}
		return mtConnect;
	}
	
	@RequestMapping(value = "mqt")
	@ResponseBody
	public String mqt(HttpServletRequest request){
		
		String rtnStr = deviceService.mqt();
		
		return rtnStr;
	}
	
	String setLimitStr(String input,int limit){
		String rtnStr;
		if(input == null
				|| input.equals(CommonCode.MSG_UNAVAIL)
				|| input.equals("NaN")
			) {
			rtnStr = null;
		}else{
			int tmpLength = input.length();
			rtnStr = input.substring(0, (tmpLength > limit)?limit:tmpLength);
		}
		
		return rtnStr;
	}
	
}
