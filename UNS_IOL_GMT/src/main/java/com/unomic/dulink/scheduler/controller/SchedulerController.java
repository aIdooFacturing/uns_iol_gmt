package com.unomic.dulink.scheduler.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.factory911.adapter.service.AdapterService;
import com.unomic.factory911.device.domain.DeviceVo;
import com.unomic.factory911.device.service.DeviceService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/sch")
@Controller
public class SchedulerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerController.class);
	
	@Autowired
	private DeviceService deviceService;

	@Autowired
	private AdapterService adapterService;

	@RequestMapping(value="spTest")
	@ResponseBody
	public void spTest(){
		callThreadTest();
	}
	
	@Scheduled(fixedDelay = 30000)
	public void callThreadTest(){
		LOGGER.info("Call IOL SCH");
		List<DeviceVo> listDvc = new ArrayList<DeviceVo>();
		listDvc = deviceService.getListIOL();
		
		if (null==listDvc){
			LOGGER.error("IOL LIST NULL ERROR");
		}
		
		for(int i = 0,size=listDvc.size() ; i<size ;i++){
			IolThread t1 = new IolThread(listDvc.get(i));
			t1.run();
		}
	}
	
//	@Scheduled(fixedDelay = 60000)
	public void callAddTC(){
		addTimeChart();
	}

	public void addTimeChart(){
		DeviceVo inputVo = new DeviceVo();
		inputVo.setTargetDate(CommonFunction.unixTime2Datetime(System.currentTimeMillis()));
		deviceService.addTimeChart(inputVo);
	}
	
	
	public class IolThread extends Thread{
		//Random rd = new Random();
		DeviceVo dvcVo;
		
		public IolThread(DeviceVo inputVo){
			dvcVo = inputVo;
		}
		
		public void run(){
			
			//test code.
//			System.out.println("IOL test1"+System.currentTimeMillis());
//			try {
//				Thread.sleep(rd.nextInt(1000));
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			//LOGGER.info("dvc_id:"+dvcVo.getDvcId());
			deviceService.setIOL(dvcVo);
		}
	}
	
}



