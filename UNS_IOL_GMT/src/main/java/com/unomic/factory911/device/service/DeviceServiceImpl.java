package com.unomic.factory911.device.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.factory911.adapter.domain.AdapterVo;
import com.unomic.factory911.device.domain.DeviceStatusVo;
import com.unomic.factory911.device.domain.DeviceVo;

@Service
@Repository
public class DeviceServiceImpl implements DeviceService{

	private final static String ADAPTER_SPACE= "com.factory911.adapter.";
	private final static String DEVICE_SPACE= "com.factory911.device.";
	
	private static final Logger logger = LoggerFactory.getLogger(DeviceServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;

	
	@Override
	@Transactional(value="txManager_ma")
	public String editLastDvcStatus(AdapterVo pureVo)
	{
		
		if(pureVo.getSpdLd() == null || pureVo.getSpdLd().equals(CommonCode.MSG_UNAVAIL)) {
			pureVo.setSpdLd(null);
		}
		
		if(pureVo.getFdOvrd() == null || pureVo.getFdOvrd().equals(CommonCode.MSG_UNAVAIL)) {
			pureVo.setFdOvrd(null);
		}
		
		if(pureVo.getChartStatus() == null || pureVo.getChartStatus().equals(CommonCode.MSG_UNAVAIL)) {
			pureVo.setChartStatus(null);
		}

		//didn't need. Calculate at server.
		//setVo.setPARAM_AVR_CYCLE_TIME((pureVo.getAvrCycleTime().equals(CommonCode.MSG_UNAVAIL))?null:pureVo.getAvrCycleTime());
		
//		if( pureVo.getChartStatus() != null && pureVo.getPrgmHead().length()>50){
//			pureVo.setPrgmHead(pureVo.getPrgmHead().substring(0, 50));
//		}else{
//			pureVo.setPrgmHead(pureVo.getPrgmHead());
//		}
		
		pureVo.setPrgmHead(setLimitStr(pureVo.getPrgmHead(),50));
		pureVo.setMainPrgmName(setLimitStr(pureVo.getMainPrgmName(),20));
		pureVo.setAlarmMsg1(setLimitStr(pureVo.getAlarmMsg1(),100));
		pureVo.setAlarmMsg2(setLimitStr(pureVo.getAlarmMsg2(),100));
		pureVo.setAlarmMsg3(setLimitStr(pureVo.getAlarmMsg3(),100));
		pureVo.setAlarmNum1(setLimitStr(pureVo.getAlarmNum1(),50));
		pureVo.setAlarmNum2(setLimitStr(pureVo.getAlarmNum2(),50));
		pureVo.setAlarmNum3(setLimitStr(pureVo.getAlarmNum3(),50));
		pureVo.setMdlM1(setLimitStr(pureVo.getMdlM1(),11));
		pureVo.setMdlM2(setLimitStr(pureVo.getMdlM2(),11));
		pureVo.setMdlM3(setLimitStr(pureVo.getMdlM3(),11));
		
//		int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE + "cntDvcLast", pureVo);
//		if(cntDvc >= 1){
//			//update
//			sql_ma.update(DEVICE_SPACE + "editDvcLast", pureVo);
//		}// else{
//			//insert
		sql_ma.delete(DEVICE_SPACE + "rmDvcLast", pureVo);
		sql_ma.insert(DEVICE_SPACE + "addDvcLast", pureVo);

		return "OK";
	}
	
	
	//pureVo 諛쏆븘�꽌 留덉�留� �븣�엺 肄붾뱶, 硫붿꽭吏� 媛��졇�삤湲�
	//�븘�슂 �뾾�쓬.
	private DeviceVo getLastAlarm(AdapterVo inputVo){
		logger.info("inputVo:"+inputVo);
		DeviceVo rtnVo = new DeviceVo();
		
		List<String> listAlarmCode = new ArrayList<String>();
		List<String> listAlarmMsg = new ArrayList<String>();
		
		if(! inputVo.getAlarmNum1().equals(CommonCode.MSG_UNAVAIL)){
			listAlarmCode.add(inputVo.getAlarmNum1());
			listAlarmMsg.add(inputVo.getAlarmMsg1());
		}
		
		if(! inputVo.getAlarmNum2().equals(CommonCode.MSG_UNAVAIL)){
			listAlarmCode.add(inputVo.getAlarmNum2());
			listAlarmMsg.add(inputVo.getAlarmMsg2());
		}
		if(! inputVo.getAlarmNum3().equals(CommonCode.MSG_UNAVAIL)){
			listAlarmCode.add(inputVo.getAlarmNum3());
			listAlarmMsg.add(inputVo.getAlarmMsg3());
		}
		
		if(listAlarmCode.size() > 1){
			rtnVo.setLastAlarmCode(listAlarmCode.get(listAlarmCode.size()-1));
			rtnVo.setLastAlarmMsg(listAlarmMsg.get(listAlarmMsg.size()-1));
		}else{
			rtnVo.setLastAlarmCode(CommonCode.MSG_UNAVAIL);
			rtnVo.setLastAlarmMsg(CommonCode.MSG_UNAVAIL);
		}
		
		return rtnVo;
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String calcDeviceOptime(DeviceVo dvcVo)
	{
		logger.info("@@@@@run calcDeviceOptime@@@@@");
		sql_ma.update(DEVICE_SPACE + "editCalcOptime",dvcVo);
		
		return "OK";
	}

	@Override
	@Transactional(value="txManager_ma")
	public String calcDvcOpPf(DeviceVo inputVo){
		
		int cnt = (int) sql_ma.selectOne(DEVICE_SPACE+"cntOpPf", inputVo);
		if(cnt>0){
			sql_ma.delete(DEVICE_SPACE+"rmvOpPf", inputVo);
		}
		sql_ma.insert(DEVICE_SPACE+"calcOpPf", inputVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String calcDeviceTimes(DeviceVo dvcVo)
	{
		logger.info("@@@@@run calcDeviceOptime@@@@@");
		
		//sql.update(DEVICE_SPACE + "editCalcOptime",dvcVo);
		sql_ma.update(DEVICE_SPACE + "editCalcIncycleTimeSP",dvcVo);
		sql_ma.update(DEVICE_SPACE + "editCalcWaitTime",dvcVo);
		sql_ma.update(DEVICE_SPACE + "editCalcAlarmTime",dvcVo);
		sql_ma.update(DEVICE_SPACE + "editCalcNoconnectionTime",dvcVo);
		
		//must update after editCalcIncycleTime
		sql_ma.update(DEVICE_SPACE + "editCalcCuttingTimeSP",dvcVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addDvcSummary(DeviceVo inputVo)
	{
		logger.info("@@@@@run addDvcSummary@@@@@");
		sql_ma.update(DEVICE_SPACE +"addDvcSummarySP", inputVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String calcDeviceTimesTest(DeviceVo dvcVo)
	{
		logger.info("@@@@@run calcDeviceOptime@@@@@");
		sql_ma.update(DEVICE_SPACE +"editCalcIncycleTimeSP",dvcVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addTimeChart(DeviceVo inputVo)
	{
		sql_ma.insert(DEVICE_SPACE +"addTimeChartSP",inputVo);
		
		return "OK";
	}
	
	/*
	 * �옉�뾽 �닚�꽌.
	 * 1. �옣鍮꾩� 留덉�留� �넻�떊 �떆媛꾩씠 2遺� �씠�긽�씠硫댁꽌 �긽�깭媛� no-connection�씠 �븘�땶 �옣鍮� 由ъ뒪�듃 �쉷�뱷.
	 * 2. adt 留덉�留� �긽�깭�뱾 endtime update �븯怨� no-connection 吏묒뼱 �꽔�쓬.
	 * 3. deviceLast no-connection�쑝濡� update.
	 *
	 * 異붽��맆 �궡�슜.
	 * 1. tb_dvc_status 留덉�留� endTime update.
	 * 2. no-connection inset.
	 */
	
	@Override
	@Transactional(value="txManager_ma")
	public String editDvcLastNoCon_SP()
	{
		sql_ma.update(DEVICE_SPACE + "editDvcLastNoCon_SP");
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String adjustDeviceChartStatus_SP(DeviceStatusVo inputVo)
	{
		sql_ma.update(DEVICE_SPACE +"adjustDeviceChartStatus_SP", inputVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addDvcChartStatus(AdapterVo inputVo)
	{
		sql_ma.update(DEVICE_SPACE +"addDvcChartStatusSP", inputVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public AdapterVo getIdfromIp(AdapterVo inputVo)
	{
		AdapterVo rtnVo = (AdapterVo) sql_ma.selectOne(DEVICE_SPACE +"getIdfromIp", inputVo);
		
		return rtnVo;
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addDvcStatics(DeviceVo inputVo)
	{
		logger.error("@@@ Run addDvcStatics");
		 sql_ma.insert(DEVICE_SPACE +"addDvcStatics_SP", inputVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String updateMcPrgmAvg()
	{
		 sql_ma.update(DEVICE_SPACE +"updateMcPrgmAvg");
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String mqt()
	{
		 String rtnStr = (String) sql_ma.selectOne(DEVICE_SPACE +"multiQueryTest");
		
		return rtnStr;
	}
	
	
	// �셿
	@Override
	@Transactional(value="txManager_ma")
	public String editDvcLastTime(String sender)
	{
		String rtnStr = "FAIL";
		DeviceVo setVo = new DeviceVo();

		setVo.setDvcId(sender);
		
		int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE + "cntDvcLast", setVo);
		if(cntDvc > 0){
			//update
			sql_ma.update(DEVICE_SPACE + "editDvcLastTime", setVo);
			rtnStr = "OK";
		}else{
			rtnStr = "UNKNOWN SENDER";
		}
		return rtnStr;
	}
	
	String setLimitStr(String input,int limit){
		String rtnStr;
		if(input == null
				|| input.equals(CommonCode.MSG_UNAVAIL)
				|| input.equals("NaN")
			) {
			rtnStr = null;
		}else{
			int tmpLength = input.length();
			rtnStr = input.substring(0, (tmpLength > limit)?limit:tmpLength);
		}
		
		return rtnStr;
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public List<DeviceVo> getListIOL()
	{
		List<DeviceVo> listDevice = sql_ma.selectList(DEVICE_SPACE + "getListIOL");
		
		return listDevice;
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String setIOL(DeviceVo inputVo){
		
		// �슂泥� 蹂대깂.
		// �뜲�씠�꽣 �꽔湲�.
		// -- AdaterStatus
		// -- DeviceLast
		DeviceVo tmpVo = inputVo;
		try {
			//logger.info(arg0);
			URL url = new URL(CommonCode.MSG_HTTP + tmpVo.getIp() + CommonCode.MSG_IOL_URL);
			URLConnection con = url.openConnection();
			con.setConnectTimeout(CommonCode.CONNECT_TIMEOUT);
			con.setReadTimeout(CommonCode.READ_TIMEOUT);
			InputStream in = con.getInputStream();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strTemp = org.apache.commons.io.IOUtils.toString(br);
			
			AdapterVo pureVo = getIOLStatus(strTemp,tmpVo.getDvcId());

			DeviceVo setVo = new DeviceVo();
			setVo.setDvcId(tmpVo.getDvcId());
			setVo.setLastUpdateTime(pureVo.getStartDateTime());
			setVo.setLastChartStatus(pureVo.getChartStatus());
			setVo.setLastStartDateTime(pureVo.getStartDateTime());
			
			//editLastStatusIOL(setVo);
			
			AdapterVo preIOLVo = getLastInputData(pureVo);
			preIOLVo = chkIOLStatus(preIOLVo);
			
			AdapterVo startVo = chkDateStarterIOL(preIOLVo, pureVo);
			if(null != startVo){
				pureVo.setStartDateTime(startVo.getEndDateTime());;
			}
			editLastEndTime(pureVo);
			addIOLStatus(pureVo);
			
		} catch (SocketTimeoutException ex) {
			logger.error("[ADT_ID:"+tmpVo.getDvcId()+"@@]"+"IOL NO-CONNECTION:"+"[IP:"+ tmpVo.getIp()+"]");
			
		} catch (Exception e){
			logger.error("[ADT_ID:"+tmpVo.getDvcId()+"@@]"+"IOL Error");
			e.printStackTrace();
		}
		
		return "OK";
	}
	
	public AdapterVo getIOLStatus(String getResult,String adtId){
		AdapterVo pureVo = new AdapterVo();
		
		String[] array;
		array = getResult.split("=|\\<");
		
		// logger.info("adtId : " + adtId);
		
		if(adtId.equals("9")) {
//			logger.info("999");
			pureVo.setDvcId(adtId);
			pureVo.setIoAlarm	((null != array[1] && null != array[3] && null != array[5] && array[1].equals("0") && array[3].equals("1") && array[5].equals("1")) ? true : false );	// ALARM
			pureVo.setIoInCycle	((null != array[1] && null != array[3] && null != array[5] && array[1].equals("1") && array[3].equals("1") && array[5].equals("0")) ? true : false );// IN-CYCLE
		    pureVo.setIoWait	((null != array[1] && null != array[3] && null != array[5] && array[1].equals("1") && array[3].equals("1") && array[5].equals("1")) ? true : false );	// WAIT
//		    logger.info("Alarm : " + pureVo.getIoAlarm());
//		    logger.info("InCycle : " + pureVo.getIoInCycle());
//		    logger.info("Wait : " + pureVo.getIoWait());
		} else {
//			logger.info("666");
			pureVo.setDvcId(adtId);
			pureVo.setIoInCycle((null != array[1] && array[1].equals("1")) ? true : false );// IN-CYCLE
			pureVo.setIoWait((null != array[3] && array[3].equals("1")) ? true : false );	// WAIT
			pureVo.setIoAlarm((null != array[5] && array[5].equals("1")) ? true : false );	// ALARM
//			logger.info("Alarm : " + pureVo.getIoAlarm());
//		    logger.info("InCycle : " + pureVo.getIoInCycle());
//		    logger.info("Wait : " + pureVo.getIoWait());
		}
	    
	    pureVo.setStatus(array[1]+array[3]+array[5]);
	    
//	    if(pureVo.getIoPower()==false){
//	    	pureVo.setChartStatus(CommonCode.MSG_NO_CONNECTION);
//	    }else
	    if(pureVo.getIoAlarm()){
	    	pureVo.setChartStatus(CommonCode.MSG_ALARM);
	    }else if (pureVo.getIoWait()){
	    	pureVo.setChartStatus(CommonCode.MSG_WAIT);
	    }else if(pureVo.getIoInCycle()){
	    	pureVo.setChartStatus(CommonCode.MSG_IN_CYCLE);
	    }else{
	    	pureVo.setChartStatus(CommonCode.MSG_WAIT);
	    }
	    
	    long crntMillTime = System.currentTimeMillis();
	    
	    pureVo.setStartDateTime(CommonFunction.unixTime2Datetime(crntMillTime));
	    pureVo.setWorkDate(CommonFunction.mil2WorkDate(crntMillTime));
	    
		return pureVo;
	}
	
	public String editLastStatusIOL(DeviceVo inputVo)
	{
		//sql_ma.update(DEVICE_SPACE + "editDvcLastStatusIOL", inputVo);
//		sql_ma.delete(DEVICE_SPACE + "rmDvcLastStatusIOL", inputVo);
//		sql_ma.insert(DEVICE_SPACE + "addDvcLastStatusIOL", inputVo);
		sql_ma.update(DEVICE_SPACE + "editDvcLastStatusIOL", inputVo);
		

		return "OK";
	}
	
	public AdapterVo chkIOLStatus(AdapterVo inputVo){
		
		String adtId = inputVo.getDvcId();
		String ioStatus = inputVo.getStatus();
		if(ioStatus == null || ioStatus.length() != CommonCode.IOL_STATUS_LENGTH){
			return inputVo;
		}

		inputVo.setIoPower((ioStatus.charAt(0)=='1') ? true : false);
		inputVo.setIoInCycle((ioStatus.charAt(1)=='1') ? true : false);
		inputVo.setIoAlarm((ioStatus.charAt(2)=='1') ? true : false);
		inputVo.setIoWait((ioStatus.charAt(3)=='1') ? true : false);
		
	    if(inputVo.getIoAlarm()){
	    	inputVo.setChartStatus(CommonCode.MSG_ALARM);
	    }else if(inputVo.getIoWait()){
	    	inputVo.setChartStatus(CommonCode.MSG_WAIT);
	    }else{
	    	inputVo.setChartStatus(CommonCode.MSG_IN_CYCLE);
	    }
	    
		return inputVo;
	}

	public AdapterVo getLastInputData(AdapterVo inputVo){
		AdapterVo newVo = new AdapterVo();
		newVo.setDvcId(inputVo.getDvcId());
		
		Integer cnt = (Integer) sql_ma.selectOne(ADAPTER_SPACE + "cntAdapterStatus",inputVo);
		
		if(1 < cnt){
			newVo  = (AdapterVo) sql_ma.selectOne(ADAPTER_SPACE + "getLastAdapterStatus",inputVo);
		}
		return newVo;
	}
	
	public AdapterVo chkDateStarterIOL(AdapterVo preVo, AdapterVo crtVo){

		if(CommonFunction.isDateStart(preVo.getStartDateTime(), crtVo.getStartDateTime())){
			logger.error("@@@@@RUN DateStarter IOL@@@@@");
			logger.error("startDateTime:"+preVo.getStartDateTime());
			logger.error("endDateTime:"+crtVo.getStartDateTime());
			
			AdapterVo starterVo = new AdapterVo();
			starterVo.setDvcId(crtVo.getDvcId());
			starterVo.setStartDateTime(CommonFunction.getStandardHourToday());
			starterVo.setEndDateTime(CommonFunction.getStandardP1SecToday());
			starterVo.setChartStatus(CommonCode.MSG_DATE_START);
			starterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));
			//list.add(i, starterVo);
			editLastEndTime(starterVo);
			addIOLStatus(starterVo);

			return starterVo;
		}
		return null;
	}
	
	public String editLastEndTime(AdapterVo firstOfListVo)
	{
		int cnt = (int) sql_ma.selectOne(ADAPTER_SPACE + "cntAdapterStatus", firstOfListVo);
		if(cnt < 1){
			return "OK";
		}
		
		AdapterVo tmpAdapterVo = (AdapterVo) sql_ma.selectOne(ADAPTER_SPACE + "getLastStartTime", firstOfListVo);
		if(tmpAdapterVo != null){
			sql_ma.delete(ADAPTER_SPACE + "removeExceptionEndTime",tmpAdapterVo);
		}
		tmpAdapterVo.setEndDateTime(firstOfListVo.getStartDateTime());
		sql_ma.update(ADAPTER_SPACE + "editLastEndTime", tmpAdapterVo);
		
		
		
		return "OK";
	}
	
	public String addIOLStatus(AdapterVo pureStatusVo)
	{
		/*System.out.println("dvcId : " + pureStatusVo.getDvcId());
		System.out.println("startTime : " + pureStatusVo.getStartDateTime());
		System.out.println("endTime : " + pureStatusVo.getStartDateTime());
		System.out.println("status : " + pureStatusVo.getStatus());
		System.out.println("chartStatus : " + pureStatusVo.getChartStatus());
		System.out.println("workDate : " + pureStatusVo.getWorkDate());*/

		sql_ma.insert(ADAPTER_SPACE+"addIOLData", pureStatusVo);
		return "OK";
	}
	
}
